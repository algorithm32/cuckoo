public class Cuckoo{
    int M;
    final int Table = 2;
    Node[][] T;

    public Cuckoo() {
        this.M = 11;
        T = new Node[Table][M];
        for(int i = 0;i<M;i++) {
            for(int j=0;j<Table;j++) {
                T[i][j] = null;
            }
        }
    }

    public Cuckoo(int size) {
        this.M = size;
        T = new Node[Table][M];
        for(int i = 0;i<M;i++) {
            for(int j=0;j<Table;j++) {
                T[j][i] = null;
            }
        }
    }

    class Node {
        int key;
        String val;

        Node(int key, String val) {
            this.key = key;
            this.val = val;
        }
    }
    private int hashIndex(int table, int key) {
        switch(table) {
            case 1 : {
                return key % M;
            }
            case 2 : {
                return (key/M) % M;
            }
        }
        return 0;
    }

    public void put(int key, String val) {
        if (T[0][hashIndex(1, key)] != null && T[0][hashIndex(1, key)].key == key) {
            T[0][hashIndex(1, key)].val = val;
            return;
        }
        if (T[1][hashIndex(2, key)] != null && T[1][hashIndex(2, key)].key == key) {
            T[1][hashIndex(2, key)].val = val;
            return;
        }
  
    }

    public String get(int key) {
        if (T[0][hashIndex(1, key)] != null && T[0][hashIndex(1, key)].key == key) {
            return T[0][hashIndex(1, key)].val;
        }
        if (T[1][hashIndex(2, key)] != null && T[1][hashIndex(2, key)].key == key) {
            return T[1][hashIndex(2, key)].val;
        }
        return null;
    }

    public void remove(int key) {
        if (T[0][hashIndex(1, key)] != null && T[0][hashIndex(1, key)].key == key) {
            T[0][hashIndex(1, key)] = null;
        }
        if (T[1][hashIndex(2, key)] != null && T[1][hashIndex(2, key)].key == key) {
            T[1][hashIndex(2, key)] = null;
        }
    }

    public void showTable() {
        for (int i = 1; i < Table + 1; i++) {
            for (int j = 0; j < M; j++) {
                if (T[i - 1][j] != null) {
                    System.out.println("[" + i + "," + j + "] = " + T[i - 1][j].key + " , " + T[i - 1][j].val);
                } else {
                    System.out.println("[" + i + "," + j + "] = is null");
                }
                System.out.println("");
            }
        }
    }



}